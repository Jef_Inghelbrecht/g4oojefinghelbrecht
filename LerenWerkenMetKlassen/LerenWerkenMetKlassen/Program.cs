﻿using System;

namespace LerenWerkenMetKlassen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            VulLijstMetProgrammeertaalNamen("typ namen van programmeertalen in: ", "De lijst met ingetypte programmeertalen: ");

            VulLijstMetProgrammeertaalNamen("typ namen van functionele programmeertalen in: ", "De lijst met ingetypte functionele programmeertalen: ");

              Console.ReadKey();
        }

        static void VulLijstMetProgrammeertaalNamen(string startMessage, string endMessage)
        {
            Console.Write(startMessage);
            ProgrammeertaalNamen programmeertaalNamen =
                new ProgrammeertaalNamen();
            for (byte i = 0; i < 5; i++)
            {
                string programmeertaalNaam = Console.ReadLine();
                programmeertaalNamen.Add(programmeertaalNaam);
            }
            Console.WriteLine(endMessage);
            Console.Write(programmeertaalNamen.PrintStringArray());
        }
    }
}
