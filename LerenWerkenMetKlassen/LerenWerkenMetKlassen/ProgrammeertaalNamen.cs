﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LerenWerkenMetKlassen
{
    class ProgrammeertaalNamen
    {
        public string programmeertaalNaam;
        private string[] programmeertaalNamen = new string[5];
        byte teller = 0;

        public string PrintStringArray()
        {
            string output = string.Empty;
            for (byte i = 0; i < programmeertaalNamen.Length; i++)
            {
                output += $"{programmeertaalNamen[i]}\n";
            }
            return output;
        }

        public void Add(string programmeertaalNaam)
        {
            programmeertaalNamen[teller++] = programmeertaalNaam;
        }

    }
}
